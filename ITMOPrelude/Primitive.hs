{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.Primitive where

import Prelude (Show,Read)

-------------------------------------------
-- Операции над функциями.
-- Определены здесь, но использовать можно и выше

infixr 9 .
f . g = \ x -> f (g x)

infixr 0 $
f $ x = f x
-------------------------------------------
-- Примитивные типы

-- Тип с единственным элементом
data Unit = Unit deriving (Show,Read)

-- Пара, произведение
data Pair a b = Pair { fst :: a, snd :: b } deriving (Show,Read)

-- Вариант, копроизведение
data Either a b = Left a | Right b deriving (Show,Read)

-- Частый частный случай, изоморфно Either Unit a
data Maybe a = Nothing | Just a deriving (Show,Read)

-- Частый частный случай, изоморфно Either Unit Unit
data Bool = False | True deriving (Show,Read)

-- Следует отметить, что встроенный if с этим Bool использовать нельзя,
-- зато case всегда работает.

-- Ну или можно реализовать свой if
if' True a b = a
if' False a b = b

-- Трихотомия. Замечательный тип, показывающий результат сравнения
data Tri = LT | EQ | GT deriving (Show,Read)

triEq :: Tri -> Tri -> Bool
triEq LT LT = True
triEq EQ EQ = True
triEq GT GT = True
triEq x y = False

infix 4 ==
(==) :: Tri -> Tri -> Bool
x == y = triEq x y


-------------------------------------------
-- Булевы значения

-- Логическое "НЕ"
not :: Bool -> Bool
not True = False
not False = True

infixr 3 &&
-- Логическое "И"
(&&) :: Bool -> Bool -> Bool
True  && x = x
False && _ = False

infixr 2 ||
-- Логическое "ИЛИ"
(||) :: Bool -> Bool -> Bool
True  || _ = True
False || x = x

-- Натуральные числа (без 0)

data PeanoNat = One | Succ PeanoNat deriving (Show, Read)

peanoOne = One
peanoTwo = Succ One
peanoThree = Succ peanoTwo
peanoFour = Succ peanoThree
peanoFive = Succ peanoFour 
peanoSix = Succ peanoFive
peanoSeven = Succ peanoSix
peanoEight = Succ peanoSeven


-- Сравнивает два натуральных числа
peanoCmp :: PeanoNat -> PeanoNat -> Tri
peanoCmp One One = EQ
peanoCmp (Succ _) One = GT
peanoCmp One (Succ _) = LT
peanoCmp (Succ n) (Succ m) = peanoCmp n m

peanoEq :: PeanoNat -> PeanoNat -> Bool
peanoEq a b = ((peanoCmp a b) == EQ) 

peanoLt :: PeanoNat -> PeanoNat -> Bool
peanoLt a b = ((peanoCmp a b) == LT)

infixl 6 +.
-- Сложение для натуральных чисел
(+.) :: PeanoNat -> PeanoNat -> PeanoNat
One +. m = Succ m
(Succ n) +. m = Succ (n +. m)

infixl 7 *.
-- Умножение для натуральных чисел
(*.) :: PeanoNat -> PeanoNat -> PeanoNat
One *. m = m
(Succ n) *. m = m +. (n *. m)


-- Неотрицательные числа

data Nat = Zero | Nat PeanoNat deriving (Show, Read)
natZero = Zero
natOne = Nat peanoOne
natTwo = Nat peanoTwo
natThree = Nat peanoThree
natFour = Nat peanoFour
natFive = Nat peanoFive
natSix = Nat peanoSix
natSeven = Nat peanoSeven
natEight = Nat peanoEight

natCmp :: Nat -> Nat -> Tri
natCmp Zero Zero = EQ
natCmp Zero (Nat _) = LT
natCmp (Nat _) Zero = GT
natCmp (Nat n) (Nat m) = peanoCmp n m

natEq :: Nat -> Nat -> Bool
natEq n m = ((natCmp n m) == EQ)

natLt :: Nat -> Nat -> Bool
natLt n m = ((natCmp n m) == LT)

infixl 6 .+, .-
(.+) :: Nat -> Nat -> Nat
Zero .+ n = n
n .+ Zero = n
Nat n .+ Nat m = Nat (n +. m)

(.-) :: Nat -> Nat -> Nat
n .- Zero = n
(Nat One) .- (Nat One) = Zero
(Nat (Succ n)) .- (Nat One) = (Nat n) 
(Nat (Succ n)) .- (Nat (Succ m)) = (Nat n) .- (Nat m)
 
infixl 7 .*
(.*) :: Nat -> Nat -> Nat
Zero .* n = Zero
n .* Zero = Zero
Nat n .* Nat m = Nat (n *. m)

-- Целое и остаток от деления n на m
natDivMod :: Nat -> Nat -> Pair Nat Nat
natDivMod n m = if' (natLt n m) (Pair Zero n) ans 
	where ans = Pair (natOne .+ (fst tmpRes)) (snd tmpRes) 
		where tmpRes = natDivMod (n .- m) m
natDiv n = fst . natDivMod n -- Целое
natMod n = snd . natDivMod n -- Остаток

-- Поиск GCD алгоритмом Евклида (должен занимать 2 (вычислителельная часть) + 1 (тип) строчки)
gcd :: Nat -> Nat -> Nat
gcd Zero a = a
gcd a Zero = a 
gcd a b = if' (natLt a b) (gcd a (natMod b a)) (gcd (natMod a b) b)

-- Знак
data Sign = Neg | Pos deriving (Show, Read)

-- Целые числа
data Int = Zero' | Int Sign PeanoNat deriving (Show, Read)
intZero = Zero'
intOne = Int Pos peanoOne
intNegOne = Int Neg peanoOne

-- n -> - n
intNeg :: Int -> Int
intNeg Zero' = Zero'
intNeg (Int Pos n) = Int Neg n
intNeg (Int Neg n) = Int Pos n

intCmp :: Int -> Int -> Tri
intCmp Zero' Zero' = EQ
intCmp Zero' (Int Pos n) = LT 
intCmp Zero' (Int Neg n) = GT 
intCmp (Int Pos n) Zero' = GT 
intCmp (Int Neg n) Zero' = LT
intCmp (Int Pos n) (Int Neg m) = GT 
intCmp (Int Neg m) (Int Pos n) = LT 
intCmp (Int Pos m) (Int Pos n) = peanoCmp m n 
intCmp (Int Neg m) (Int Neg n) = peanoCmp n m 


intEq :: Int -> Int -> Bool
intEq n m = ((intCmp n m) == EQ)

intLt :: Int -> Int -> Bool
intLt n m = ((intCmp n m) == LT)

infixl 6 .+., .-.
(.+.) :: Int -> Int -> Int
n .+. Zero' = n
Zero' .+. n = n
(Int Pos n) .+. (Int Pos m) = Int Pos (n +. m) 
(Int Neg n) .+. (Int Neg m) = Int Neg (n +. m)
(Int Pos One) .+. (Int Neg One) = Zero'  
(Int Pos One) .+. (Int Neg (Succ m)) = Int Neg m
(Int Pos (Succ n)) .+. (Int Neg One) = Int Pos n
(Int Pos (Succ n)) .+. (Int Neg (Succ m)) = (Int Pos n) .+. (Int Neg m)
(Int Neg n) .+. (Int Pos m) = (Int Pos m) .+. (Int Neg n)

(.-.) :: Int -> Int -> Int
n .-. m = n .+. (intNeg m)

infixl 7 .*.
(.*.) :: Int -> Int -> Int
Zero' .*. n = Zero'
n .*. Zero' = Zero'
(Int Pos n) .*. (Int Pos m) = Int Pos (n *. m)
(Int Neg n) .*. (Int Neg m) = Int Pos (n *. m)
(Int Neg n) .*. (Int Pos m) = Int Neg (n *. m)
(Int Pos n) .*. (Int Neg m) = Int Neg (n *. m)


-- Целое и остаток от деления n на m
intDiv :: Int -> Int -> Int
intDiv (Int sign1 n) (Int sign2 m) = if' (peanoLt n m) Zero' ans 
	where ans = intOne .+. tmpRes 
		where tmpRes = intDiv ((Int Pos n) .-. (Int Pos m)) (Int Pos m)
intDiv Zero' (Int sign n) = Zero'

-------------------------------------------
-- Рациональные числа

data Rat = Rat Int PeanoNat deriving (Show, Read)

ratNeg :: Rat -> Rat
ratNeg (Rat x y) = Rat (intNeg x) y

-- У рациональных ещё есть обратные элементы
ratInv :: Rat -> Rat
ratInv (Rat (Int sign x) y) = Rat (Int sign y) x


nat2peano :: Nat -> PeanoNat
nat2peano (Nat a) = a

--Упрощение
ratSimpl (Rat (Int sig x) y) =
   let ux = Nat x in
   let uy = Nat y in
   let k = gcd (ux) (uy) in
   let sx = natDiv ux k in
   let sy = natDiv uy k in
   Rat (Int sig (nat2peano sx)) (nat2peano sy)

ratComDen :: Rat -> Rat -> Pair Rat Rat
ratComDen (Rat x1 y1) (Rat x2 y2) =
	let g = gcd (Nat y1) (Nat y2) in
   let ig = Int Pos (nat2peano g) in
	let den = y1 *. y2 in
	let num1 = x1 .*. (Int Pos y2) in
	let num2 = x2 .*. (Int Pos y1) in
	let sden = natDiv (Nat den) g in
	let snum1 = intDiv num1 ig in
	let snum2 = intDiv num2 ig in
	let a = Rat snum1 (nat2peano sden) in
	let b = Rat snum2 (nat2peano sden) in
	Pair a b

ratCmp :: Rat -> Rat -> Tri
ratCmp a b =
   let com = ratComDen a b in
   let comCmp = \(Pair (Rat x1 y1) (Rat x2 y2)) -> intCmp x1 x2 in
   comCmp com

ratEq :: Rat -> Rat -> Bool
ratEq a b = (ratCmp a b) == EQ

ratLt :: Rat -> Rat -> Bool
ratLt a b = (ratCmp a b) == LT

infixl 7 %+, %-
(%+) :: Rat -> Rat -> Rat
a %+ b =
   let com = ratComDen a b in
   let comAdd = \(Pair (Rat x1 y1) (Rat x2 y2)) -> Rat (x1 .+. x2) y1 in
   comAdd com

(%-) :: Rat -> Rat -> Rat
n %- m = n %+ (ratNeg m)

infixl 7 %*, %/
(%*) :: Rat -> Rat -> Rat
(Rat x1 y1) %* (Rat x2 y2) =
   let unsimp = Rat (x1 .*. x2) (y1 *. y2) in
   ratSimpl unsimp

(%/) :: Rat -> Rat -> Rat
n %/ m = n %* (ratInv m)
