{-# LANGUAGE NoImplicitPrelude #-}
module ITMOPrelude.List where

import Prelude (Show,Read,error)
import ITMOPrelude.Primitive

---------------------------------------------
-- Что надо делать?
--
-- Все undefined превратить в требуемые термы.
-- Звёздочкой (*) отмечены места, в которых может потребоваться думать.

---------------------------------------------
-- Определение

data List a = Nil |  Cons a (List a) deriving (Show,Read)

---------------------------------------------
-- Операции

-- Длина списка
length :: List a -> Nat
length Nil = Zero
length (Cons a b) = natOne .+ (length b)

-- Склеить два списка за O(length a)
(++) :: List a -> List a -> List a
Nil ++ a = a
(Cons a b) ++ c = Cons a (b ++ c)

-- Список без первого элемента
tail :: List a -> List a
tail Nil = error "bad argument"
tail (Cons a b) = b

-- Список без последнего элемента
init :: List a -> List a
init Nil = error "bad argument"
init (Cons a Nil)  = Nil
init (Cons a b) = Cons a (init b)

-- Первый элемент
head :: List a -> a
head Nil = error "bad argument"
head (Cons a b) = a

-- Последний элемент
last :: List a -> a
last Nil = error "bad argument"
last (Cons a Nil) = a
last (Cons a b) = last b

-- n первых элементов списка
take :: Nat -> List a -> List a
take Zero a = Nil
take n Nil = Nil
take n (Cons a b) = Cons a (take (n .- natOne) b)

-- Список без n первых элементов
drop :: Nat -> List a -> List a
drop Zero a = a
drop n Nil = Nil
drop n (Cons a b) = drop (n .- natOne) b

-- Оставить в списке только элементы удовлетворяющие p
filter :: (a -> Bool) -> List a -> List a
filter p Nil = Nil
filter p (Cons a b) = if' (p a) (Cons a (filter p b)) (filter p b) 

-- Обобщённая версия. Вместо "выбросить/оставить" p
-- говорит "выбросить/оставить b".
gfilter :: (a -> Maybe b) -> List a -> List b
gfilter p Nil = Nil
gfilter p a =
   let t = map p a in
   let p x = case x of
               Nothing -> False
               (Just x) -> True in
   let t2 = filter p t in
   let unpack x = case x of
                     Nothing -> error "Logic eror"
                     Just a -> a
   in map unpack t2



-- Применяет f к каждому элементу списка
map :: (x -> y) -> List x -> List y
map p Nil = Nil
map p (Cons a b) = Cons (p a) (map p b) 


-- Копировать из списка в результат до первого нарушения предиката
-- takeWhile (< 3) [1,2,3,4,1,2,3,4] == [1,2]
takeWhile :: (a -> Bool) -> List a -> List a
takeWhile = undefined

-- Не копировать из списка в результат до первого нарушения предиката,
-- после чего скопировать все элементы, включая первый нарушивший
-- dropWhile (< 3) [1,2,3,4,1,2,3,4] == [3,4,1,2,3,4]
dropWhile :: (a -> Bool) -> List a -> List a
dropWhile = undefined

